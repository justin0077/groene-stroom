window.onload = function() {

  //Schermen
  var startScherm =                                               document.getElementById("js--startScherm");
  var eersteScherm =                                              document.getElementById("js--scherm1");
  var groeneStroom =                                              document.getElementById("js--groeneStroom");
  var statusScherm =                                              document.getElementById("js--statusScherm");
  var doelstellingen =                                            document.getElementById("js--doelstellingen");
  var quiz1Scherm =                                               document.getElementById("js--quiz1");
  var hoofdstuk2Scherm =                                          document.getElementById("js--hoofdstuk2Scherm");
  var groeneTrein =                                               document.getElementById("js--groeneTreinScherm");
  var groeneTrein2 =                                              document.getElementById("js--groeneTreinScherm2");
  var laatsteInformatieScherm =                                   document.getElementById("js--laatsteInformatieScherm");
  var quiz2Scherm =                                               document.getElementById("js--quiz2");
  var laatsteScherm =                                             document.getElementById("js--laatsteScherm");

  //Overige Schermen
  var moreScherm =                                                document.getElementsByClassName("js--moreMenu");
  var popUp =                                                     document.getElementsByClassName('popUp');

  //Buttons
  var back =                                                      document.getElementsByClassName("Back");
  var begin =                                                     document.getElementById("js--begin");
  var startButton =                                               document.getElementById("js--startButton");
  var moreButton =                                                document.getElementsByClassName("js--more");
  var stopButton =                                                document.getElementsByClassName("js--stoppen");
  var sluitButton =                                               document.getElementsByClassName("js--sluiten");
  var quizButton =                                                document.getElementsByClassName("js--quizButton");
  var hoofdstuk2 =                                                document.getElementById("js--hoofdstuk2");
  var groeneTreinButton =                                         document.getElementById("js--groeneTreinStart");
  var groeneTreinAnimatie =                                       document.getElementById("js--trein-animatie");
  var einde =                                                     document.getElementById("js--einde");
  var restart =                                                   document.getElementById("js--Restart");

  var footer =                                                    document.getElementById("js--footer");

  var forward =                                                   document.getElementsByClassName("forward");

  var treinFoto =                                                 document.getElementById("js--treinAnimatie");

  //geluid
  var snd =                                                       new Audio("images/stoomfluit2.ogg");
  var tick =                                                      new Audio("images/tick.mp3");
  var tick2 =                                                     new Audio("images/sidemenu.mp3");

  //Overige informatie
  var treinInfo =                                                 document.getElementById("js--treinInfo");
  var treinInfo2 =                                                document.getElementById("js--treinInfo2");



  // Hier zitten de functies in voor het submenu
  var i =                                                         moreButton;
  var j =                                                         moreScherm;
  var k =                                                         stopButton;
  var l =                                                         sluitButton;

  // Hier wordt de functie uitgevoerd wanneer op het icoon geklikt wordt
  for(i = 0; i < moreButton.length; i++){
    moreButton[i].onclick = function() {
      for(j = 0; j < moreScherm.length; j++){
        moreScherm[j].style.display =                             "block";
        moreScherm[j].style.zIndex =                              "2";
      }
    };
  }
  //Dit is de functie voor wanneer er op de stop knop gelikt wordt
  for(k = 0; k < stopButton.length; k++){
    stopButton[k].onclick = function() {
      window.location.reload();
    };
  }
  //Dit is de functie om het Menu weer te sluiten
  for(l = 0; l < sluitButton.length; l++){
    sluitButton[l].onclick = function() {
      for(j = 0; j < moreScherm.length; j++){
        moreScherm[j].style.display =                             "none";
      }
    };
  }


  var g = back;
  for(g = 0; g < back.length; g++){
    back[g].onclick = function() {
    };
  }


  // Hier wordt het submenu geactiveerd
  $(document).on('click', 'ul li', function() {
  	$(this).addClass('active').siblings().removeClass('active');
    tick2.play();
  });



  // dit is de onclick om bij scherm 1 verder te gaan
  startButton.onclick = function() {
    startScherm.style.display =                                   "none";
    eersteScherm.style.display =                                  "block";
    tick.play();
  };

  // dit is om terug te gaan naar het startscherm
  back[0].onclick = function() {
    eersteScherm.style.display = "none";
    startScherm.style.display = "block";
  };

  // dit is de onclick om bij scherm 3 verder te gaan
  begin.onclick = function() {
    eersteScherm.style.display =                                  "none";
    groeneStroom.style.display =                                  "block";
    tick.play();
  };

  // dit is de onclick om bij scherm 4 verder te gaan
  forward[0].onclick = function(){
    groeneStroom.style.display =                                  "none";
    statusScherm.style.display =                                  "block";
    tick.play();
  };

  // dit is om terug te gaan naar het eerste subkopje
  back[1].onclick = function() {
    statusScherm.style.display =                                  "none";
    groeneStroom.style.display =                                  "block";
  };

  //dit is de onclick om bij scherm 5 verder te gaan
  forward[1].onclick = function() {
    statusScherm.style.display =                                  "none";
    doelstellingen.style.display =                                "block";
    tick.play();
  };

  // dit is om terug te gaan naar het tweede subkopje
  back[2].onclick = function() {
    doelstellingen.style.display =                                "none";
    statusScherm.style.display =                                  "block";
  };

  //dit is de onclick om bij scherm 6 verder te gaan
  forward[2].onclick = function() {
    popUp[0].style.display =                                      "block";
    quizButton[0].style.backgroundColor =                         "#f7a841";
    quizButton[0].style.marginTop =                               "30px";
    tick.play();
  };

  //dit is de onclick om bij de eerste quiz verder te gaan
  quizButton[0].onclick = function() {
    popUp[0].style.display =                                      "none";
    doelstellingen.style.display =                                "none";
    quiz1Scherm.style.display =                                   "block";
    tick.play();
  };

  //dit is de onclick om door te gaan naar hoofdstuk 2
  hoofdstuk2.onclick = function() {
    quiz1Scherm.style.display =                                   "none";
    hoofdstuk2Scherm.style.display =                              "block";
    groeneTreinButton.style.backgroundColor =                     "#8bc34a";
    tick.play();
  };

  //dit is het scherm om te starten bij de groene Trein
  groeneTreinButton.onclick = function() {
    hoofdstuk2Scherm.style.display =                              "none";
    groeneTrein.style.display =                                   "block";
    tick.play();
  };

  //dit is de onclick om bij groene Trein verder te gaan
  forward[3].onclick = function() {
    groeneTrein.classList.add("hidden");
    groeneTrein.style.display =                                    "none";
    groeneTrein2.style.display =                                   "block";
    tick.play();
  };

  //dit is de onclick om bij groene Trein naar het laatste scherm te gaan
  forward[4].onclick = function() {
    groeneTrein2.style.display =                                   "none";
    laatsteInformatieScherm.style.display =                        "block";
    tick.play();
  };

  // dit is om terug te gaan naar het tweede subkopje van hoofdstuk 2
  back[3].onclick = function() {
    laatsteInformatieScherm.style.display =                        "none";
    groeneTrein2.style.display =                                   "block";
  };

  //hiermee gaan we naar de laatste quiz
  forward[5].onclick = function() {
    popUp[1].style.display =                                       "block";
    quizButton[1].style.backgroundColor =                          "#f7a841";
    quizButton[1].style.marginTop =                                "30px";
    tick.play();
  };

  quizButton[1].onclick = function() {
    popUp[1].style.display =                                       "none";
    quiz2Scherm.style.display =                                    "block";
    laatsteInformatieScherm.style.display =                        "none";
    tick.play();
  };

  einde.onclick = function() {
    popUp[1].style.display =                                       "none";
    quiz2Scherm.style.display =                                    "none";
    laatsteScherm.style.display =                                  "block";
    tick.play();
  };

  restart.onclick = function() {
    window.location.reload();
  };



  //dit is de trein animatie
  groeneTreinAnimatie.onclick = function() {
    treinFoto.classList.add("treinAnimatie");
    groeneTreinAnimatie.style.display =                             "none";
    snd.play();
    treinInfo.classList.add("treinInfo");
    treinInfo.style.display =                                       "block";
    treinInfo2.classList.add("treinInfo2");
    treinInfo2.style.display =                                      "block";
    footer.classList.remove("footer");
    footer.classList.add("footer-trein");
    footer.style.display =                                          "block";
  };

};



//Hieronder is te zien hoe de eerste quiz werkt
var pos = 0, test, test_status, question, choice, choices, chA, chB, chC, chD, correct = 0;
var pos2 = 0, test, test_status, question, choice, choices, chA, chB, chC, chD, correct = 0;

var questions = [
  ["Hoeveel procent gebruikt Nederland aan fossiele brandstoffen?", "92", "93", "91", "95", 'B'],
  ["Welke fossiele brandstoffen wordt het meest gebruikt in Nederland?", "Aardolie", "Steenkool", "Aardgas", "Bruinkool", "C"],
  ["Op welk van de vier duurzame energieën is men het meest tegen?", "Waterkracht", "Windenergie", "Zonne-energie ", "Kernenergie", "D"],
  ["Hoeveel kolencentrales wil Nederland in het jaar 2035 nog hebben?", "0", "3", "1", "2", "A"]
  ];

var questions2 = [
  ["Waar gaat de opgewekte stroom op zee als eerste naar toe?", "Spanningstation (zee)", "De trein", "naar het land", "Spanningstation (land)", 'A'],
  ["Sinds welk jaar rijden alle treinen in Nederland op groene energie?", "2015", "2014", "2017", "2018", "D"],
  ["Welke stad verbruikt er evenveel Tera watt als de NS?", "Den Haag", "Haarlem", "Amsterdam", "Rotterdam", "C"],
  ["Welk openbaar vervoersmiddel wordt het meest gebruikt?", "Bus", "Metro", "Tram", "Trein", "D"]
  ];

function get(x){
  return document.getElementById(x);
}

function renderQuestion(){
  test = get("test");
  if(pos >= questions.length){
    document.getElementById("test").innerHTML = "<h2>Je hebt "+correct+" van de "+questions.length+" vragen goed!</h2>";
    document.getElementById("test_status_titel").innerHTML = "Einde van de test";
    if(correct == 0) {
      document.getElementById("uitslag").innerHTML = "Jammer! als je iets beter gelezen had dan had je er zeker meer goed gehad!" + "<br><br>" + "Klik op de knop om door te gaan naar het volgende hoofdstuk!";
    }
    else if (correct == 1) {
      document.getElementById("uitslag").innerHTML = "Jammer! De volgende keer heb je er vast meer dan 1 goed!" + "<br><br>" + "Klik op de knop om door te gaan naar het volgende hoofdstuk!";
    }
    else if (correct == 2) {
      document.getElementById("uitslag").innerHTML = "Goedzo! Je hebt de helft goed!" + "<br><br>" + "Klik op de knop om door te gaan naar het volgende hoofdstuk!";
    }
    else if (correct == 3) {
      document.getElementById("uitslag").innerHTML = "Goedzo! Bijna allemaal goed!" + "<br><br>" + "Klik op de knop om door te gaan naar het volgende hoofdstuk!";
    }
    else {
      document.getElementById("uitslag").innerHTML = "Wauw! Je hebt alles goed!" + "<br><br>" + "Klik op de knop om door te gaan naar het volgende hoofdstuk!";
    }
    document.getElementById("js--hoofdstuk2").innerHTML = "<button id='js--hoofdstuk2' class='begin'>VOLGENDE</button>";
    pos = 0;
    correct = 0;
    return false;
  }

  document.getElementById("test_status_titel").innerHTML = "Vraag "+(pos+1)+" van de "+questions.length;
  question = questions[pos][0];
  chA = questions[pos][1];
  chB = questions[pos][2];
  chC = questions[pos][3];
  chD = questions[pos][4];
  document.getElementById("test").innerHTML = "<h3>"+question+"</h3>";
  // the += appends to the data we started on the line above
  document.getElementById("test").innerHTML += "<input type='radio' class='option-input radio' name='choices' value='A'> "+chA+"<br>";
  document.getElementById("test").innerHTML += "<input type='radio' class='option-input radio' name='choices' value='B'> "+chB+"<br>";
  document.getElementById("test").innerHTML += "<input type='radio' class='option-input radio' name='choices' value='C'> "+chC+"<br>";
  document.getElementById("test").innerHTML += "<input type='radio' class='option-input radio' name='choices' value='D'> "+chD+"<br><br>";
  document.getElementById("test").innerHTML += "<footer class='test'><button class='submit' onclick='checkAnswer()'>SUBMIT</button></footer>";
}

function renderQuestion2(){
  test2 = get("test2");
  if(pos2 >= questions2.length){
    document.getElementById("test2").innerHTML = "<h2>Je hebt "+correct+" van de "+questions2.length+" vragen goed!</h2>";
    document.getElementById("test_status2_titel").innerHTML = "Einde van de test";
    if(correct == 0) {
      document.getElementById("uitslag2").innerHTML = "Jammer! als je iets beter gelezen had dan had je er zeker meer goed gehad!" + "<br><br>" + "Dit was het einde van de tweede quiz. Klik op de knop om door te gaan naar het einde!";
    }
    else if (correct == 1) {
      document.getElementById("uitslag2").innerHTML = "Jammer! De volgende keer heb je er vast meer dan 1 goed!" + "<br><br>" + "Dit was het einde van de tweede quiz. Klik op de knop om door te gaan naar het einde!";
    }
    else if (correct == 2) {
      document.getElementById("uitslag2").innerHTML = "Goedzo! Je hebt de helft goed!" + "<br><br>" + "Dit was het einde van de tweede quiz. Klik op de knop om door te gaan naar het einde!";
    }
    else if (correct == 3) {
      document.getElementById("uitslag2").innerHTML = "Goedzo! Bijna allemaal goed!" + "<br><br>" + "Dit was het einde van de tweede quiz. Klik op de knop om door te gaan naar het einde!";
    }
    else {
      document.getElementById("uitslag2").innerHTML = "Wauw! Je hebt alles goed!" + "<br><br>" + "Dit was het einde van de tweede quiz. Klik op de knop om door te gaan naar het einde!";
    }
    document.getElementById("js--einde").innerHTML = "<button id='js--einde' class='begin'>VOLGENDE</button>";
    pos2 = 0;
    correct = 0;
    return false;
  }

  document.getElementById("test_status2_titel").innerHTML = "Vraag "+(pos2+1)+" van de "+questions2.length;
  question = questions2[pos2][0];
  chA = questions2[pos2][1];
  chB = questions2[pos2][2];
  chC = questions2[pos2][3];
  chD = questions2[pos2][4];
  document.getElementById("test2").innerHTML = "<h3>"+question+"</h3>";
  // the += appends to the data we started on the line above
  document.getElementById("test2").innerHTML += "<input type='radio' class='option-input radio' name='choices' value='A'> "+chA+"<br>";
  document.getElementById("test2").innerHTML += "<input type='radio' class='option-input radio' name='choices' value='B'> "+chB+"<br>";
  document.getElementById("test2").innerHTML += "<input type='radio' class='option-input radio' name='choices' value='C'> "+chC+"<br>";
  document.getElementById("test2").innerHTML += "<input type='radio' class='option-input radio' name='choices' value='D'> "+chD+"<br><br>";
  document.getElementById("test2").innerHTML += "<footer class='test'><button class='submit' onclick='checkAnswer2()'>SUBMIT</button></footer>";
}

function checkAnswer(){
  choices = document.getElementsByName("choices");
  for(var i=0; i<choices.length; i++){
    if(choices[i].checked){
      choice = choices[i].value;
    }
  }
  if(choice == questions[pos][5]){
    correct++;
  }
  pos++;
  renderQuestion();
}

function checkAnswer2(){
  choices = document.getElementsByName("choices");
  for(var i=0; i<choices.length; i++){
    if(choices[i].checked){
      choice = choices[i].value;
    }
  }
  if(choice == questions2[pos2][5]){
    correct++;
  }
  pos2++;
  renderQuestion2();
}

window.addEventListener("load", renderQuestion, false);
window.addEventListener("load", renderQuestion2, false);
